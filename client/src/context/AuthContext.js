import React, { useState, createContext } from "react";

const AuthContext = createContext();
const { Provider } = AuthContext;

const AuthProvider = ({ children }) => { 

  const userData = localStorage.getItem('userData') || {}

  const [authState, setAuthState] = useState({
    userData: userData.username ? JSON.parse(userData) : {}
  });

  const setAuthInfo = (userData) =>{
    localStorage.setItem(
      'userData', JSON.stringify(userData)
    );
    setAuthState({
      userData: {'first_name': userData.first_name, 'username': userData.username, 'email': userData.email}
    })
  }

  const isAuthenticated = () => {
    if(!authState.username){
      return false;
    }
  }

  return (
    <Provider
      value={{
        authState, 
        setAuthState: authInfo => setAuthInfo(authInfo), 
        isAuthenticated
        }}
      >
      {children}
    </Provider>
   
  );
};

export {AuthContext, AuthProvider};