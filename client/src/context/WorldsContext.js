import React, { useState, createContext } from "react";

export const WorldsContext = createContext();

export const WorldsContextProvider = (props) => {
  const [worldData, setWorldData ] = useState([]);
  const [locationData, setLocationData ] = useState([]);
  const [selectedWorld, setSelectedWorld] = useState({});
  const [selectedLocation, setSelectedLocation] = useState({});
  const [tagData, setTagData] = useState([]);
  const [myLocations, setMyLocations] = useState([])
  const [residentData, setResidentData] = useState([])

  const addWorlds = (world) => {
    setWorldData([...worldData, world]);
  };

  const addLocations = (location) => {
    setLocationData([...locationData, location]);
  };
  
  
  return (
    <WorldsContext.Provider
      value={{
        worldData,
        setWorldData,
        addWorlds,
        selectedWorld,
        setSelectedWorld,
        locationData,
        setLocationData,
        selectedLocation,
        setSelectedLocation,
        addLocations,
        tagData, 
        setTagData,
        residentData, 
        setResidentData,
        myLocations, setMyLocations
      }}
    >
      {props.children}
    </WorldsContext.Provider>
  );
};