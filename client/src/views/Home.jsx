import React, { useContext, useEffect } from "react";
import { publicFetch } from "../apis/fetch";
import { WorldsContext } from "../context/WorldsContext";
import { AuthContext } from "../context/AuthContext";

import Panel from "../components/Panel";
import Search from "../components/Search";
import LocationList from "../components/LocationList";
import BiomeList from "../components/BiomeList";
import Header from "../components/Header";
import LocationThumbnail from "../components/LocationThumbnail";

const Home = (props) => {
  const {
    setWorldData,
    selectedWorld,
    setTagData,
    removeLocalStorage,
    locationIndex,
    setLocationIndex,
    setMyLocations,
    setBiomes,
    setSelectedLocation,
  } = useContext(WorldsContext);

  const { userData, isLoggedIn } = useContext(AuthContext);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await publicFetch.get("/");
        setLocationIndex(response.data.data.all_locations);
        setWorldData(response.data.data.world_data);
        setTagData(response.data.data.tags);
        setBiomes(response.data.data.biomes);
        setMyLocations(response.data.data.my_locations);
      } catch (err) {
        console.log(err);
      }
    };
    fetchData();
  }, [isLoggedIn]);

  useEffect(() => {
    removeLocalStorage("selectedLocation");
    setSelectedLocation({});
  }, []);

  return (
    <>
      <Header />
      <main className="homePage">
        <Panel />
        <section className="main-container">
          <Search />
          <div className="locationListContainer">
            <h2>Recently Added</h2>
            <ul className="locationList">
              {locationIndex.length > 0 &&
                locationIndex.map((location) => {
                  return <LocationThumbnail location={location} />;
                })}

              {/* {Object.entries(userData).length !== 0 && <li className='addLocationLink'><Link to={`/worlds/${selectedWorld.id}/locations/new`}>Add New Location</Link></li>} */}
            </ul>
          </div>

          <BiomeList />
        </section>
      </main>
    </>
  );
};

export default Home;
