import React from 'react'
import { useLocation } from "react-router-dom";
import SearchResult from '../components/SearchResult';

const SearchResults = (searchResults) => {
  const location = useLocation();

  console.log(location.state)
  return (
      <ul className="searchResults">
        {location.state.length > 0 &&
          location.state.map((result) => {
            return <SearchResult location={result} />;
          })}
      </ul>
  )
}

export default SearchResults
