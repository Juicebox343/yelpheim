import React, {useContext, useEffect, useState} from 'react'
import { publicFetch } from "../apis/fetch";
import { WorldsContext } from '../context/WorldsContext';
import {Redirect, useLocation, useHistory, Link, useParams} from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import { sendingText } from '../scripts';

const Search = () => {
  let history = useHistory();
  const { selectedWorld, tagData} = useContext(WorldsContext);

  const {userData} = useContext(AuthContext);

  const [searchResults, setSearchResults] = useState(null)
  const [userSearch, setUserSearch] = useState("")

    const doSearch = async (e, userSearch) => {
      e.preventDefault();
      try {
        const response = await publicFetch.get(`/search/${sendingText(userSearch)}`);
        // setSearchResults(response.data.data.results);
        history.push(`/search`, response.data.data.results);
      } catch (err) {
        console.log(err);
      }
    };

  return (
    <div className='searchContainer'>
        <form>
            <h2><label htmlFor='locationSearch'>Search {selectedWorld && selectedWorld.world_name}</label></h2>
            <div><input type='text' value={userSearch} onChange={e=> setUserSearch(e.target.value)} id='locationSearch' placeholder='Bed, Black Forest, Forge (Level 5)'></input><button class='attentionButton' type='submit' onClick={(e) => doSearch(e, userSearch) }>Search</button></div>
            <ul className='truncatedTagList'>
                {tagData.length > 0 && <li><Link to={`/tags/${tagData[2].tag_name}`}>{tagData[2].tag_name}</Link></li>}
                {tagData.length > 0 && <li><Link to={`/tags/${tagData[15].tag_name}`}>{tagData[15].tag_name}</Link></li>}
                {tagData.length > 0 && <li><Link to={`/tags/${tagData[8].tag_name}`}>{tagData[8].tag_name}</Link></li>}
                {tagData.length > 0 && <li><Link to={`/tags/${tagData[21].tag_name}`}>{tagData[21].tag_name}</Link></li>}
            </ul>
        </form>
    </div>
  )
}

export default Search
