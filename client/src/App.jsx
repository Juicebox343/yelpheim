import React, {useContext} from 'react';
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';

import Home from "./routes/Home";
import { WorldsContextProvider } from './context/WorldsContext';
import { AuthProvider} from './context/AuthContext';
import LocationDetailPage from './routes/LocationDetailPage';
import LocationUpdatePage from './routes/LocationUpdatePage';
import LoginPage from './routes/LoginPage';
import RegistrationPage from './routes/RegistrationPage';
import WorldUpdatePage from './routes/WorldUpdatePage';
import NotFoundPage from './routes/NotFoundPage';
import "./main.css";
import AddLocationPage from './routes/AddLocationPage';
import AddWorldPage from './routes/AddWorldPage';
import { authContext } from './context/AuthContext';

const App = () => {

    const loggedInState = false;
// figure out how to redirect people from the login page to profile page if they're logged in
    return(
        <AuthProvider>
            <WorldsContextProvider>
                <div className=''>
                    <Router>
                        {loggedInState ? 
                            <Switch>
                                <Route exact path='/:username' component={Home}/>
                                <Route exact path='/:username/worlds/:world_id/update' component={WorldUpdatePage}/> 
                                <Route exact path='/:username/worlds/:world_id/locations/:location_id/update' component={LocationUpdatePage}/> 
                                <Route exact path='/:username/worlds/:world_id/locations/new' component={AddLocationPage}/> 
                                <Route exact path='/:username/worlds/:world_id/locations/:location_id' component={LocationDetailPage}/> 
                                <Route exact path='/:username/worlds/new' component={AddWorldPage}/> 
                                <Route component={NotFoundPage}/>
                            </Switch>
                            :
                            <Switch>
                                <Route exact path='/' component={LoginPage}/>
                                <Route exact path='/register' component={RegistrationPage}/>
                            </Switch>
                            }
                    </Router>
                </div>
            </WorldsContextProvider> 
        </AuthProvider>
    )
}


export default App;